module gitlab.com/pw-order-of-devs/go/go-validators

go 1.18

require (
	github.com/BurntSushi/toml v1.1.0
	github.com/magiconair/properties v1.8.6
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
