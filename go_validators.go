package go_validators

import (
	"encoding/json"
	"fmt"
	"github.com/BurntSushi/toml"
	"gopkg.in/yaml.v3"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"strings"
)

var rules = make(map[rule]bool)

type rule struct {
	structure string
	field     string
	regex     string
	label     *string
}

// Add rule

func getDefaultBool(value []bool, defaultValue bool) bool {
	if len(value) == 0 {
		return defaultValue
	} else {
		return value[0]
	}
}

type IAddRule interface {
	addRule(skipIfInvalid ...bool) error
}

func tryCompileRegex(regex string, skipIfInvalid ...bool) error {
	if !getDefaultBool(skipIfInvalid, false) {
		regexp.MustCompile(regex)
	}
	_, err := regexp.Compile(regex)
	return err
}

type AddRuleStr struct {
	item  string
	field string
	regex string
	label *string
}

func (ar AddRuleStr) addRule(skipIfInvalid ...bool) error {
	err := tryCompileRegex(ar.regex, getDefaultBool(skipIfInvalid, false))
	if err == nil {
		rules[rule{ar.item, ar.field, ar.regex, ar.label}] = true
	}
	return err
}

type AddRuleRef struct {
	item  reflect.Type
	field string
	regex string
	label *string
}

func (ar AddRuleRef) addRule(skipIfInvalid ...bool) error {
	err := tryCompileRegex(ar.regex, getDefaultBool(skipIfInvalid, false))
	if err == nil {
		rules[rule{ar.item.Name(), ar.field, ar.regex, ar.label}] = true
	}
	return err
}

func AddRule(item IAddRule, skipIfInvalid ...bool) error {
	return item.addRule(getDefaultBool(skipIfInvalid, false))
}

func AddRules(items []IAddRule, skipIfInvalid ...bool) []error {
	errors := make([]error, 0)
	for _, _rule := range items {
		err := AddRule(_rule, getDefaultBool(skipIfInvalid, false))
		if err != nil {
			errors = append(errors, err)
		}
	}
	if len(errors) == 0 {
		return nil
	} else {
		return errors
	}
}

var supportedFileTypes = []string{".yml", ".yaml", ".json", ".toml"}

func stringEndsWith(str string, suffixes []string) bool {
	for _, s := range suffixes {
		if strings.HasSuffix(str, s) {
			return true
		}
	}
	return false
}

func parseFileInternal(validator map[string]interface{}, skipIfInvalid ...bool) []error {
	errors := make([]error, 0)
	for _, expression := range validator["expressions"].([]interface{}) {
		var nLabel *string = nil
		if validator["label"] != nil {
			_nLabel := validator["label"].(string)
			nLabel = &_nLabel
		}
		nRule := AddRuleStr{
			validator["structure"].(string),
			validator["field"].(string),
			expression.(string),
			nLabel,
		}
		err := nRule.addRule(getDefaultBool(skipIfInvalid, false))
		if err != nil {
			errors = append(errors, err)
		}
	}
	return errors
}

func parseFileStruct(_rules map[string]interface{}, skipIfInvalid ...bool) []error {
	errors := make([]error, 0)
	for _, validator := range _rules["validators"].([]interface{}) {
		validator := validator.(map[string]interface{})
		errors = append(errors, parseFileInternal(validator, getDefaultBool(skipIfInvalid, false))...)
	}
	return errors
}

func parseFileMap(_rules map[string]interface{}, skipIfInvalid ...bool) []error {
	errors := make([]error, 0)
	for _, validator := range _rules["validators"].([]map[string]interface{}) {
		errors = append(errors, parseFileInternal(validator, getDefaultBool(skipIfInvalid, false))...)
	}
	return errors
}

func AddRulesFromFile(path string, skipIfInvalid ...bool) []error {
	skipIfInvalidValue := getDefaultBool(skipIfInvalid, false)
	errors := make([]error, 0)
	if !stringEndsWith(path, supportedFileTypes) {
		return []error{fmt.Errorf("file error: supported extensions are %v", supportedFileTypes)}
	}

	file, err := os.ReadFile(path)
	if err != nil {
		return []error{err}
	}
	_rules := make(map[string]interface{})
	if stringEndsWith(path, []string{".yml", ".yaml"}) {
		err = yaml.Unmarshal(file, &_rules)
		if err == nil {
			errors = append(errors, parseFileStruct(_rules, skipIfInvalidValue)...)
		}
	} else if stringEndsWith(path, []string{".json"}) {
		err = json.Unmarshal(file, &_rules)
		if err == nil {
			errors = append(errors, parseFileStruct(_rules, skipIfInvalidValue)...)
		}
	} else if stringEndsWith(path, []string{".toml"}) {
		err = toml.Unmarshal(file, &_rules)
		if err == nil {
			errors = append(errors, parseFileMap(_rules, skipIfInvalidValue)...)
		}
	}
	if err != nil {
		return []error{err}
	}

	if len(errors) == 0 {
		return nil
	} else {
		return errors
	}
}

func walkOverPath(path string) ([]string, error) {
	files := make([]string, 0)
	err := filepath.Walk(path, func(p string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		} else if stringEndsWith(p, supportedFileTypes) {
			files = append(files, p)
		}
		return nil
	})
	if err != nil {
		return nil, err
	} else {
		return files, nil
	}
}

func AddRulesFromPath(path string, skipIfInvalid ...bool) []error {
	skipIfInvalidValue := getDefaultBool(skipIfInvalid, false)
	errors := make([]error, 0)
	files, err := walkOverPath(path)
	if err != nil {
		return []error{err}
	}

	for _, file := range files {
		errors = append(errors, AddRulesFromFile(file, skipIfInvalidValue)...)
	}

	if len(errors) == 0 {
		return nil
	} else {
		return errors
	}
}

// Validate

func isPrimitive(item reflect.Value) bool {
	return item.Kind().String() == item.Type().String()
}

func parseValue(item any) reflect.Value {
	if reflect.ValueOf(item).Type() == reflect.TypeOf(reflect.Value{}) {
		return item.(reflect.Value)
	} else {
		return reflect.ValueOf(item)
	}
}

func getFieldValue(item reflect.Value, field *string) string {
	if field == nil {
		return item.String()
	} else {
		return item.FieldByName(*field).String()
	}
}

func validateSingle(item reflect.Value, field *string, _rule rule, r *regexp.Regexp) ([]string, bool) {
	value := getFieldValue(item, field)
	structure := item.Type().Name()
	errors := make([]string, 0)
	if structure == _rule.structure && !r.MatchString(value) {
		errors = append(errors, fmt.Sprintf(
			"validation for field \"%s\" with value \"%s\" failed for expression \"%s\"",
			_rule.field, value, _rule.regex,
		))
	}
	return errors, len(errors) == 0
}

func unique(slice []string) []string {
	keys := make(map[string]bool)
	var list []string
	for _, entry := range slice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func validateInternal(errors []string, reflected reflect.Value, _rule rule) []string {
	r := regexp.MustCompile(_rule.regex)
	if isPrimitive(reflected) {
		errorsPartial, _ := validateSingle(reflected, nil, _rule, r)
		errors = append(errors, errorsPartial...)
	} else {
		for i := 0; i < reflected.NumField(); i++ {
			field := reflected.Field(i)
			if field.Kind() == reflect.Struct {
				errorsPartial, _ := Validate(field)
				errors = append(errors, errorsPartial...)
			} else if field.Kind() != reflect.Invalid {
				errorsPartial, _ := validateSingle(reflected, &_rule.field, _rule, r)
				errors = append(errors, errorsPartial...)
			}
		}
	}
	return errors
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func filter(s map[rule]bool, f []string) map[rule]bool {
	result := make(map[rule]bool, 0)
	for _s, _r := range s {
		if _s.label != nil && contains(f, *_s.label) {
			result[_s] = _r
		}
	}
	return result
}

func Validate(item any, label ...string) ([]string, bool) {
	if len(rules) == 0 {
		return nil, true
	}

	reflected := parseValue(item)

	errors := make([]string, 0)
	_rules := rules
	if len(label) > 0 && !contains(label, "*") {
		_rules = filter(rules, label)
	}
	for _rule := range _rules {
		errors = append(errors, validateInternal(errors, reflected, _rule)...)
	}
	return unique(errors), len(errors) == 0
}
