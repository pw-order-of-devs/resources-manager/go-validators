package go_validators

import (
	"github.com/magiconair/properties/assert"
	"reflect"
	"testing"
)

const StringLengthRule = "^\\s*(?:\\S\\s*){3,6}$"
const StringCharsRule = "^[a-zA-Z]*$"

type someitem struct {
	value string
}

func setup() {
	rules = make(map[rule]bool)
}

func TestAddRule(t *testing.T) {
	setup()
	_ = AddRule(AddRuleStr{item: "someitem", field: "value", regex: StringLengthRule}, false)
	assert.Equal(t, len(rules), 1)
	_ = AddRule(AddRuleRef{item: reflect.TypeOf(someitem{}), field: "value", regex: StringLengthRule}, false)
	assert.Equal(t, len(rules), 1)
	AddRules([]IAddRule{AddRuleStr{item: "someitem", field: "value", regex: StringLengthRule}}, false)
	assert.Equal(t, len(rules), 1)
	testLabel := "label"
	AddRules([]IAddRule{AddRuleStr{item: "someitem", field: "value", regex: StringLengthRule, label: &testLabel}}, false)
	assert.Equal(t, len(rules), 2)
}

func TestAddRulesFromFileYaml(t *testing.T) {
	setup()
	errors := AddRulesFromFile("validators/rules.yaml", false)
	assert.Equal(t, len(errors), 0)
	assert.Equal(t, len(rules), 5)
}

func TestAddRulesFromFileJson(t *testing.T) {
	setup()
	errors := AddRulesFromFile("validators/rules.json", false)
	assert.Equal(t, len(errors), 0)
	assert.Equal(t, len(rules), 5)
}

func TestAddRulesFromFileToml(t *testing.T) {
	setup()
	errors := AddRulesFromFile("validators/rules.toml", false)
	assert.Equal(t, len(errors), 0)
	assert.Equal(t, len(rules), 5)
}

func TestAddRulesFromPath(t *testing.T) {
	setup()
	errors := AddRulesFromPath("validators", false)
	assert.Equal(t, len(errors), 0)
	assert.Equal(t, len(rules), 5)
}

func TestValidate(t *testing.T) {
	setup()
	_ = AddRule(AddRuleStr{item: "someitem", field: "value", regex: StringLengthRule}, false)
	errors, ok := Validate(someitem{"test"})
	assert.Equal(t, ok, true)
	assert.Equal(t, len(errors), 0)
}

func TestValidateLabel(t *testing.T) {
	setup()
	labelLength := "length"
	labelChars := "chars"
	AddRules([]IAddRule{
		AddRuleStr{item: "someitem", field: "value", regex: StringLengthRule, label: &labelLength},
		AddRuleStr{item: "someitem", field: "value", regex: StringCharsRule, label: &labelChars},
	}, false)
	errors, ok := Validate(someitem{"test"})
	assert.Equal(t, ok, true)
	assert.Equal(t, len(errors), 0)
	errors, ok = Validate(someitem{"tt"}, labelLength)
	assert.Equal(t, ok, false)
	assert.Equal(t, len(errors), 1)
	errors, ok = Validate(someitem{"01"}, labelChars)
	assert.Equal(t, ok, false)
	assert.Equal(t, len(errors), 1)
	errors, ok = Validate(someitem{"testtest1"})
	assert.Equal(t, ok, false)
	assert.Equal(t, len(errors), 2)
}

func TestValidateError(t *testing.T) {
	setup()
	_ = AddRule(AddRuleStr{item: "someitem", field: "value", regex: StringLengthRule}, false)
	errors, ok := Validate(someitem{"test_error"})
	assert.Equal(t, ok, false)
	assert.Equal(t, len(errors), 1)
	assert.Matches(t, errors[0], "^validation for field (.*) with value (.*) failed for expression (.*)$")
}

type someNestedItem struct {
	value someitem
}

func TestValidatePrimitive(t *testing.T) {
	setup()
	_ = AddRule(AddRuleStr{item: "string", field: "*", regex: StringLengthRule}, false)
	errors, ok := Validate("test")
	assert.Equal(t, ok, true)
	assert.Equal(t, len(errors), 0)
	errors, ok = Validate(1234)
	assert.Equal(t, ok, true)
	assert.Equal(t, len(errors), 0)
}

func TestValidatePrimitiveError(t *testing.T) {
	setup()
	_ = AddRule(AddRuleStr{item: "string", field: "*", regex: StringLengthRule}, false)
	errors, ok := Validate("test_error")
	assert.Equal(t, ok, false)
	assert.Equal(t, len(errors), 1)
	assert.Matches(t, errors[0], "^validation for field (.*) with value (.*) failed for expression (.*)$")
	_ = AddRule(AddRuleStr{item: "int", field: "*", regex: StringLengthRule}, false)
	errors, ok = Validate(12341234)
	assert.Equal(t, ok, false)
	assert.Equal(t, len(errors), 1)
	assert.Matches(t, errors[0], "^validation for field (.*) with value (.*) failed for expression (.*)$")
}

func TestValidateNested(t *testing.T) {
	setup()
	_ = AddRule(AddRuleStr{item: "someitem", field: "value", regex: StringLengthRule}, false)
	errors, ok := Validate(someNestedItem{someitem{"test"}})
	assert.Equal(t, ok, true)
	assert.Equal(t, len(errors), 0)
}

func TestValidateNestedError(t *testing.T) {
	setup()
	_ = AddRule(AddRuleStr{item: "someitem", field: "value", regex: StringLengthRule}, false)
	errors, ok := Validate(someNestedItem{someitem{"test_error"}})
	assert.Equal(t, ok, false)
	assert.Equal(t, len(errors), 1)
	assert.Matches(t, errors[0], "^validation for field (.*) with value (.*) failed for expression (.*)$")
}
