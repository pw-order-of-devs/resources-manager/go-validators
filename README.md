# Go Validators

## Table of Contents

- [About](#about)
- [Usage](#usage)

## About <a name = "about"></a>

Configurable validators for Go

## Usage <a name="usage"></a>

To get the package as dependency:

```
go get gitlab.com/pw-order-of-devs/go/go-validators
```

Example usage:

To register rule(s):
```
AddRule(AddRuleStr{"someitem", "value", "^\\s*(?:\\S\\s*){3,6}$"}, false)
AddRule(AddRuleRef{reflect.TypeOf(someitem{}), "value", "^\\s*(?:\\S\\s*){3,6}$"}, false)
AddRules([]IAddRule{
	AddRuleStr{"someitem", "value", "^\\s*(?:\\S\\s*){3,6}$"},
}, false)
AddRulesFromFile(".../validators.yaml", false) 
AddRulesFromPath(".../validators", false) 
```

To validate a struct against registered rules:
```
errors, result := Validate(someitem{"content"})
```

Example usage with [optional] label:
To register rule(s):
```
label := "label"
AddRule(AddRuleStr{"someitem", "value", "^\\s*(?:\\S\\s*){3,6}$", &label}, false)
```

To validate a struct against registered rules:
```
errors, result := Validate(someitem{"content"}, "label")
```

